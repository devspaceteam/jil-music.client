import {Component, ViewEncapsulation} from "@angular/core";
import {SettingsPanelComponent} from "../settings-panel.component";

@Component({
    selector: 'videoads-settings',
    templateUrl: './videoads-settings.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class VideoadsSettingsComponent extends SettingsPanelComponent {
}
