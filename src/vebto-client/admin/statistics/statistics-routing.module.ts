import {Route} from '@angular/router';

import {TrendsStatisticsComponent} from "./trends/trends-statistics.component";
import {SharesStatisticsComponent} from "./shares/shares-statistics.component";
import {SalesStatisticsComponent} from "./sales/sales-statistics.component";

export const vebtoStatisticsRoutes: Route[] = [
    {path: '', redirectTo: 'trends', pathMatch: 'full'},
    {path: 'trends', component: TrendsStatisticsComponent, pathMatch: 'full'},    
    {path: 'shares', component: SharesStatisticsComponent},
    {path: 'sales', component: SalesStatisticsComponent},      
];

// @NgModule({
//     imports: [RouterModule.forChild(routes)],
//     exports: [RouterModule]
// })
// export class SettingsRoutingModule {}