export class VideoAd {
	id: number;
	name: string;
	url: string;
	status: string;
	duration?: number;
		
	constructor(params: Object = {}) {
        for (let name in params) {
            this[name] = params[name];
        }
    }
}