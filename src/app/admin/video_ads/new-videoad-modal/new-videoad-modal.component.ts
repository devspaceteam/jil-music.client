import {Component, Inject, Optional, ViewEncapsulation} from '@angular/core';
import {Settings} from "vebto-client/core/config/settings.service";
import {VideoAds} from "../../../web-player/video_ads/video_ads.service";
import {VideoAd} from "../../../models/VideoAd";
import {Modal} from "vebto-client/core/ui/modal.service";
import {Album} from "../../../models/Album";
import {Artist} from "../../../models/Artist";
import {UploadFileModalComponent} from "vebto-client/core/files/upload-file-modal/upload-file-modal.component";
import {MAT_DIALOG_DATA, MatAutocompleteSelectedEvent, MatDialogRef} from "@angular/material";
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap} from 'rxjs/operators';
import {of as observableOf} from 'rxjs';
import {Search} from '../../../web-player/search/search.service';
import {WebPlayerImagesService} from '../../../web-player/web-player-images.service';

import {RolesToTrack} from "../../../models/RolesToTrack";

export interface CrupdateVideoadModalData {
    videoad?: VideoAd,
}

@Component({
    selector: 'new-videoad-modal',
    templateUrl: './new-videoad-modal.component.html',
    styleUrls: ['./new-videoad-modal.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NewVideoadModalComponent {

    /**
     * Backend validation errors from last create or update request.
     */
    public errors: any = {};

    /**
     * Album this videoad should be attached to.
     */
    public rolestovideoad: RolesToTrack;
    public mykey;

    /**
     * Whether we are updating or creating a videoad.
     */
    public updating = false;

    public loading = false;

    /**
     * New videoad model.
     */
    public videoad = new VideoAd();

    /**
     * NewTrackModalComponent Constructor.
     */
    constructor(
        public settings: Settings,
        protected videoads: VideoAds,
        protected modal: Modal,
        private search: Search,
        private dialogRef: MatDialogRef<NewVideoadModalComponent>,
        public images: WebPlayerImagesService,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: CrupdateVideoadModalData,
    ) {
        this.hydrate(this.data);
    }

    /**
     * Confirm videoad creation.
     */
    public confirm() {
        //editing existing videoad
        if (this.videoad.id) {
            this.update();
        }

        //creating or updating videoad for new album
        else {
            this.create();
            //this.close(this.getPayload());
        }
    }

    public close(data?: any) {
        this.dialogRef.close(data);
    }

    /**
     * Update existing videoad.
     */
    public update() {
        this.videoads.update(this.videoad.id, this.getPayload()).subscribe(videoad => {
            this.loading = false;
            this.dialogRef.close(videoad);
        }, errors => {
            this.loading = false;
            this.errors = errors.messages;
        });
    }

    /**
     * Create a new videoad.
     */
    public create() {
        this.videoads.create(this.getPayload()).subscribe(videoad => {
            this.loading = false;
            this.dialogRef.close(videoad);
        }, errors => {
            this.loading = false;
            this.errors = errors.messages;
        });
    }

    /**
     * Open modal for uploading videoad streaming file.
     */
    public openUploadMusicModal() {
        const params = {uri: 'uploads/videos', httpParams: {type: 'videoad'}};
        this.modal.show(UploadFileModalComponent, params).afterClosed().subscribe(uploadedFile => {
            if ( ! uploadedFile) return;
            this.videoad.url = uploadedFile.url;
            this.autoFillDuration(this.videoad.url);
        });
    }

    /**
     * Auto fill duration field using specified media file url.
     */
    public autoFillDuration(url: string) {
        if ( ! url) return;
        const audio = document.createElement('audio');

        audio.addEventListener('canplaythrough', (e) => {
            const target = (e.currentTarget || e.target) as HTMLMediaElement;

            if (target.duration) {
                this.videoad.duration = Math.ceil(target.duration * 1000);
            }

            audio.remove();
        });

        audio.src = url;
    }

        /**
     * Get videoad payload for backend.
     */
    private getPayload() {
        let payload = Object.assign({}, this.videoad);
        return payload;
    }

    /**
     * Hydrate videoad and album models.
     */
    private hydrate(params: CrupdateVideoadModalData) {
        //set videoad model on the modal
        if (params.videoad) this.videoad = Object.assign({}, params.videoad);

        //hydrate rolestovideoad input model
        //this.rolestovideoad = Object.assign({}, params.rolestovideoad, params.videoad ? params.videoad.rolestovideoad : {});
        
       //hydrate videoad number for new videoads
//if ( ! params.videoad) {
//const num = this.album.videoads && this.album.videoads.length;
//this.videoad.number = num ? num+1 : 1;
        //}

        this.updating = !!params.videoad;
    }
  
}
