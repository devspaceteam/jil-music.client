import {Component, ViewEncapsulation} from '@angular/core';
import {Player} from "../../player.service";

import {CurrentUser} from "vebto-client/auth/current-user";
import {Router} from '@angular/router';

declare let Android: any;

@Component({
    selector: 'repeat-button',
    templateUrl: './repeat-button.component.html',
    styleUrls: ['./repeat-button.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RepeatButtonComponent {

    public dataTrackToAndroid = '';       

    /**
     * Whether mobile layout should be activated.
     */
    public isMobile: boolean = false;

    constructor(
        public player: Player,
        protected router: Router,
        protected currentUser: CurrentUser,
    ) {this.isMobile = window.matchMedia && window.matchMedia('(max-width: 768px)').matches;}
     
    public callbackMobile() {
        
        let track_to_mobile = this.player.getCuedTrack();        
        if ( ! track_to_mobile){
            this.dataTrackToAndroid = '';
            return;
        }

        if (track_to_mobile.album.is_premium){
            if(!this.currentUser.isLoggedIn()){
                this.router.navigate(['/login']);
                return;
            }
            if(!this.currentUser.jilSubscriptionIsActive()){
                this.router.navigate(['/account/subscription']);
                return;
            }            
        }
        
        if(this.isMobile){

            console.log('Track to Download =>');
            Android.getTrackObj(JSON.stringify(track_to_mobile));  
            console.log('<= Track to Download');            
            
        }
        this.dataTrackToAndroid = JSON.stringify(track_to_mobile);
    }

}
