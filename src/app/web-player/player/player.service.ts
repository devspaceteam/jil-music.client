import {Injectable} from '@angular/core';
import {PlayerQueue} from "./player-queue.service";
import {Track} from "../../models/Track";
import {YoutubeStrategy} from "./strategies/youtube-strategy.service";
import {PlayerState} from "./player-state.service";
import {Settings} from "vebto-client/core/config/settings.service";
import {FullscreenOverlay} from "../fullscreen-overlay/fullscreen-overlay.service";
import {WebPlayerState} from "../web-player-state.service";
import {PlaybackStrategy} from "./strategies/playback-strategy.interface";
import {Html5Strategy} from "./strategies/html5-strategy.service";
import {SoundcloudStrategy} from "./strategies/soundcloud-strategy.service";
import {Subscription} from "rxjs";
import {TrackPlays} from "./track-plays.service";
import {LocalStorage} from "vebto-client/core/services/local-storage.service";
import {BrowserEvents} from "vebto-client/core/services/browser-events.service";

import {CurrentUser} from "vebto-client/auth/current-user";
import {Router} from '@angular/router';

import {Album} from "../../models/Album";
import {Albums} from "../albums/albums.service";

import {VideoAd} from "../../models/VideoAd";
import {VideoAds} from "../../web-player/video_ads/video_ads.service";
import {shuffler} from "./shuffler";

@Injectable()
export class Player {

    /**
     * Active service subscriptions.
     */
    private subscriptions: Subscription[] = [];

    /**
     * Currently active playback strategy.
     */
    private playbackStrategy: PlaybackStrategy;

    /**
     * Currently active playback strategy.
     */
    private activePlaybackStrategy: 'youtube'|'html5'|'soundcloud';

    /**
     * Current player volume.
     */
    private volume: number;

    /**
     * Whether continuous playback should be
     * handled by the player after song ends.
     */
    public handleContinuousPlayback = true;

    /**
     * Whether playback has been started via user gesture.
     *
     * If true, there's no need to maximize player overlay
     * anymore, because external controls will work properly.
     */
    private playbackStartedViaGesture = false;

    private playbackPause = false;

    public album : Album;
    
    public adsVideo = {};

    public videoAds : VideoAd[];

    /**
     * Player Constructor.
     */
    constructor(
        public queue: PlayerQueue,
        private youtube: YoutubeStrategy,
        private html5: Html5Strategy,
        private soundcloud: SoundcloudStrategy,
        private storage: LocalStorage,
        private settings: Settings,
        public state: PlayerState,
        private globalState: WebPlayerState,
        private overlay: FullscreenOverlay,
        private browserEvents: BrowserEvents,
        private trackPlays: TrackPlays,
        protected router: Router,
        protected currentUser: CurrentUser,
        protected albums: Albums, 
        protected videoAdsServise: VideoAds,       
    ) {}
    
    /**
     * Check Track in Premium Album.
     */
    public trackIsPremium(isPremium: boolean) {

        if (isPremium){
            if(!this.currentUser.isLoggedIn()){
                this.stop();
                this.router.navigate(['/login']);
            }
            if(!this.currentUser.jilSubscriptionIsActive()){
                this.stop();
                this.router.navigate(['/account/subscription']);
            }            
        }    
        
    }

    /**
     * Check Track in Premium Album.
     */
    public addAdsToPlay(track: Track) {
        
        if(this.adsVideo){            
            this.queue.removeAds(this.adsVideo);                        
            this.adsVideo = {};
        }
        
        if (!this.currentUser.isLoggedIn() || !this.currentUser.jilSubscriptionIsActive()){
            
            if(!this.videoAds){        
                this.videoAdsServise.getActive().subscribe(data => {                
                    this.videoAds = data;            
                });
            }            
            if(this.globalState.adsShow()){
                if(this.videoAds){  
                    this.videoAds = shuffler.shuffle(this.videoAds, false);                
                    let videoAdsPrepend = this.videoAds[0];                
                    this.adsVideo = {
                        id: 999999999,
                        name: videoAdsPrepend.name,
                        album_name: 'video_ads',
                        number: 0,
                        duration: videoAdsPrepend.duration,
                        youtube_id: '',
                        spotify_popularity: 0,
                        album_id: 0,
                        url: videoAdsPrepend.url,
                        plays: 0,
                    };                
                    this.queue.prependAds(this.adsVideo);
                    track = this.queue.getCurrent();
                }
            }

        }   
        return track;
    }

    /**
     * Start the playback.
     */
    public play() {
        if ( ! this.ready()) return;

        let track = this.queue.getCurrent();
        if ( ! track) return this.stop();
        
        if (track.album_name != 'video_ads'){
            if (track.album.is_premium){            
                this.trackIsPremium(track.album.is_premium);
            }else{            
                if(this.album && this.album.id == track.album.id){
                    this.trackIsPremium(this.album.is_premium);
                }else{
                    this.albums.get(track.album.id).subscribe(response  => {                
                        this.album = response;
                        this.trackIsPremium(this.album.is_premium);
                    }, response => this.trackIsPremium(false));
                }                                
            }
        }
        
        if(!this.playbackPause){
            track = this.addAdsToPlay(track);
        }else{
            this.playbackPause = false;
        }
        
        
        this.setStrategy(track);        
        this.maybeMaximizeOverlay();

        this.playbackStrategy.play();
    }

    /**
     * Pause the playback.
     */
    public pause() {
        this.playbackStrategy.pause();
        this.playbackPause = true;
    }

    /**
     * Play or pause player based on current playback state.
     */
    public togglePlayback() {        
        if (this.isPlaying()) {            
            this.pause();            
        } else {            
            this.play();
        }
    }

    /**
     * Check if current playback strategy is ready.
     */
    public ready() {
        return this.playbackStrategy.ready();
    }

    /**
     * Check if playback is in progress.
     */
    public isPlaying(): boolean {
        return this.state.playing;
    }

    /**
     * Check if player has any or specified track cued.
     */
    public cued(track?: Track) {
        const cued = this.getCuedTrack() && this.getCuedTrack().id;
        if ( ! track) return cued;
        return cued && this.getCuedTrack() === track;
    }

    /**
     * Get player state service.
     */
    public getState(): PlayerState {
        return this.state;
    }

    /**
     * Get player queue service.
     */
    public getQueue(): PlayerQueue {
        return this.queue;
    }

    /**
     * Check if player is buffering currently..
     */
    public isBuffering(): boolean {
        return this.state.buffering;
    }

    /**
     * Check if player is muted.
     */
    public isMuted(): boolean {
        return this.state.muted;
    }

    /**
     * Get track that is currently cued.
     */
    public getCuedTrack(): Track {
        if ( ! this.playbackStrategy) return null;
        return this.playbackStrategy.getCuedTrack();
    }

    /**
     * Mute player.
     */
    public mute() {
        this.playbackStrategy.mute();
        this.state.muted = true;
    }

    /**
     * Unmute player.
     */
    public unMute() {
        this.playbackStrategy.unMute();
        this.state.muted = false;
    }

    /**
     * Get current player volume.
     */
    public getVolume() {
        return this.volume;
    }

    /**
     * Set volume to a number between 0 and 100.
     */
    public setVolume(volume: number) {
        this.volume = volume;
        this.playbackStrategy.setVolume(volume);
        this.storage.set('player.volume', volume);
    }

    /**
     * Stop playback and seek to start of track.
     */
    public stop() {
        if ( ! this.state.playing) return;        
        this.playbackStrategy.pause();
        this.seekTo(0);
        this.state.playing = false;
        this.state.firePlaybackStopped();
    }

    /**
     * Get time that has elapsed since playback start.
     */
    public getCurrentTime() {
        return this.playbackStrategy.getCurrentTime();
    }

    /**
     * Get total duration of track in seconds.
     */
    public getDuration() {
        return this.playbackStrategy.getDuration();
    }

    /**
     * Seek to specified time in track.
     */
    public seekTo(time: number): Promise<any> {
        if (this.state.playing && this.getCuedTrack().album_name == 'video_ads'){            
            this.playbackStrategy.seekTo(this.getCurrentTime());  
        }else{
            this.playbackStrategy.seekTo(time);
        }         
        return new Promise(resolve => setTimeout(() => resolve(), 50));
    }

    /**
     * Toggle between repeat, repeat one and no repeat modes.
     */
    public toggleRepeatMode() {
        if (this.state.repeating) {
            this.state.repeatingOne = true;
        } else if (this.state.repeatingOne) {
            this.state.repeatingOne = false;
            this.state.repeating = false;
        } else {
            this.state.repeating = true;
        }
    }

    /**
     * Play next track in queue based on current repeat setting.
     */
    public playNext() {        
        this.playbackPause = false;

        if (this.state.playing && this.getCuedTrack().album_name == 'video_ads'){            
            return;
        }
        
        this.stop(); 
        let track = this.queue.getCurrent();

        if (track.album_name == 'video_ads'){            
            this.globalState.adsShowed();
            track = this.queue.getPrevious();            
        }else if (this.state.repeating && this.queue.isLast()) {
            track = this.queue.getFirst();            
        } else if ( ! this.state.repeatingOne) {
            track = this.queue.getNext();
        }

        this.queue.select(track);        

        this.play();
    }

    /**
     * Play previous track in queue based on current repeat setting.
     */
    public playPrevious() {        
        this.playbackPause = false;

        if (this.state.playing && this.getCuedTrack().album_name == 'video_ads'){            
            return;
        }
        this.stop(); let track = this.queue.getCurrent();

        if (this.state.repeating && this.queue.isFirst()) {
            track = this.queue.getLast();
        } else if (!this.state.repeatingOne) {
            track = this.queue.getPrevious();
        }

        this.queue.select(track);
        this.play();
    }

    /**
     * Toggle player shuffle mode.
     */
    public toggleShuffle() {        
        if (this.state.shuffling) {
            this.queue.restoreOriginal();
        } else {
            this.queue.shuffle();
        }

        this.state.shuffling = !this.state.shuffling;
    }

    /**
     * Override player queue and cue first track.
     */
    public overrideQueue(params: {tracks: Track[], queuedItemId?: number|string}, queuePointer: number = 0): Promise<any> {
        this.playbackPause = false;

        this.putQueueIntoLocalStorage(params.tracks);
        this.queue.override(params, queuePointer);

        return this.cueTrack(this.queue.getCurrent());
    }

    /**
     * Cue specified track for playback.
     */
    public cueTrack(track: Track): Promise<any> {
        this.playbackPause = false;
        
        let promise: Promise<any>;
        this.setStrategy(track);

        if ( ! track || ! this.playbackStrategy) {
            promise = new Promise(resolve => resolve());
        } else {
            this.queue.select(track);
            promise = this.playbackStrategy.cueTrack(track);
        }

        return promise.then(() => {
            this.state.buffering = false;
        });
    }

    /**
     * Get currently active playback strategy.
     */
    public getPlaybackStrategy(): string {
        return this.activePlaybackStrategy;
    }

    /**
     * Init the player.
     */
    public init() {
        this.loadStateFromLocalStorage();
        this.setStrategy(this.queue.getCurrent());
        this.setInitialVolume();
        this.cueTrack(this.queue.getCurrent());
        this.bindToPlaybackStateEvents();
        this.initKeybinds();
    }

    /**
     * Destroy the player.
     */
    public destroy() {
        this.playbackStrategy && this.playbackStrategy.destroy();
        this.state.playing = false;

        this.subscriptions.forEach(subscription => {
            subscription.unsubscribe();
        });

        this.subscriptions = [];
    }

    /**
     * Put specified queue into local storage and limit tracks to 15.
     */
    private putQueueIntoLocalStorage(tracks: Track[]) {
        if ( ! tracks) return;
        this.storage.set('player.queue', {tracks: tracks.slice(0, 15)});
    }

    /**
     * Set playback strategy based on specified track.
     */
    private setStrategy(track: Track): PlaybackStrategy {        
        if (track && track.url) {
            this.playbackStrategy = this.html5;
            this.activePlaybackStrategy = 'html5';
        } else if (this.settings.get('audio_search_provider') === 'soundcloud') {
            this.playbackStrategy = this.soundcloud;
            this.activePlaybackStrategy = 'soundcloud';
        } else {
            this.playbackStrategy = this.youtube;
            this.activePlaybackStrategy = 'youtube';
        }

        //destroy all except current active playback strategy
        if (this.activePlaybackStrategy !== 'youtube') this.youtube.destroy();
        if (this.activePlaybackStrategy !== 'html5') this.html5.destroy();
        if (this.activePlaybackStrategy !== 'soundcloud') this.soundcloud.destroy();

        return this.playbackStrategy;
    }

    private loadStateFromLocalStorage() {
        this.state.muted = this.storage.get('player.muted', false);
        this.state.repeating = this.storage.get('player.repeating', true);
        this.state.repeatingOne = this.storage.get('player.repeatingOne', false);
        this.state.shuffling = this.storage.get('player.shuffling', false);
        const queuePointer = this.storage.get('player.queue.pointer', 0);
        this.queue.override(this.storage.get('player.queue', {tracks: []}), queuePointer);
    }

    /**
     * Set initial player volume.
     */
    private setInitialVolume() {
        let defaultVolume = this.settings.get('player.default_volume', 30);
            defaultVolume = this.storage.get('player.volume', defaultVolume);

        this.setVolume(defaultVolume);
        this.html5.setVolume(defaultVolume);
    }

    /**
     * Maximize fullscreen overlay if we're on mobile,
     * because youtube embed needs to be visible to start
     * playback with external youtube iframe api controls
     */
    private async maybeMaximizeOverlay(): Promise<boolean> {
        const shouldOpen = this.settings.get('player.mobile.auto_open_overlay');

        if (this.playbackStartedViaGesture || ! shouldOpen || ! this.globalState.isMobile) return;

        await this.overlay.maximize();
        this.playbackStartedViaGesture = true;
    }

    /**
     * Play next track when current track ends.
     */
    private bindToPlaybackStateEvents() {
        this.state.onChange$.subscribe(type => {
            if (type === 'PLAYBACK_STARTED') {
                this.trackPlays.increment(this.getCuedTrack());
            } else if (type === 'PLAYBACK_ENDED' && this.handleContinuousPlayback) {
                this.trackPlays.clearPlayedTrack(this.getCuedTrack());
                this.playNext();
            }
        });
    }

    /**
     * Initiate player keyboard shortcuts.
     */
    public initKeybinds() {
        const sub = this.browserEvents.globalKeyDown$.subscribe((e: KeyboardEvent) => {
            //SPACE - toggle playback
            if (e.keyCode === this.browserEvents.keyCodes.space) {
                this.togglePlayback(); e.preventDefault();

                //ctrl+right - play next track
            } else if (e.ctrlKey && e.keyCode === this.browserEvents.keyCodes.arrowRight) {
                this.playNext(); e.preventDefault();
            }

            //ctrl+left - play previous track
            else if (e.ctrlKey && e.keyCode === this.browserEvents.keyCodes.arrowLeft) {
                this.playPrevious(); e.preventDefault();
            }
        });

        this.subscriptions.push(sub);
    }
}
